From: Derek Homeier <dhomeie@gwdg.de>
Date: Wed, 23 Jun 2021 16:54:54 +0200
Subject: Handle astropy 4.3 `PhysicalType`s

https://github.com/astropy/specutils/pull/833

This converts the new ~astropy.units.physical.PhysicalType class
from 4.3 (astropy/astropy#11204) to string, if necessary, and also
uses the appropriate type only from types like PhysicalType({'energy',
'torque', 'work'}). Updated tests to test setting of custom spectral_axis
units and types as well.
---
 specutils/io/default_loaders/tabular_fits.py |  6 ++++--
 specutils/io/parsing_utils.py                |  9 ++++-----
 specutils/tests/test_loaders.py              | 11 ++++++-----
 3 files changed, 14 insertions(+), 12 deletions(-)

diff --git a/specutils/io/default_loaders/tabular_fits.py b/specutils/io/default_loaders/tabular_fits.py
index 3e0a68a..bb05cb2 100644
--- a/specutils/io/default_loaders/tabular_fits.py
+++ b/specutils/io/default_loaders/tabular_fits.py
@@ -134,10 +134,12 @@ def tabular_fits_writer(spectrum, file_name, hdu=1, update_header=False, **kwarg
     wunit = u.Unit(kwargs.pop('wunit', spectrum.spectral_axis.unit))
     disp = spectrum.spectral_axis.to(wunit, equivalencies=u.spectral())
 
-    # Mapping of spectral_axis types to header TTYPE1
-    dispname = wunit.physical_type
+    # Mapping of spectral_axis types to header TTYPE1 (no "torque/work" types!)
+    dispname = str(wunit.physical_type)
     if dispname == "length":
         dispname = "wavelength"
+    elif "energy" in dispname:
+        dispname = "energy"
 
     # Add flux array and unit
     ftype = kwargs.pop('ftype', spectrum.flux.dtype)
diff --git a/specutils/io/parsing_utils.py b/specutils/io/parsing_utils.py
index 409f431..2fd9b79 100644
--- a/specutils/io/parsing_utils.py
+++ b/specutils/io/parsing_utils.py
@@ -100,17 +100,16 @@ def spectrum_from_column_mapping(table, column_mapping, wcs=None):
 
             # Attempt to convert the table unit to the user-defined unit.
             log.debug("Attempting auto-convert of table unit '%s' to "
-                          "user-provided unit '%s'.", tab_unit, cm_unit)
+                      "user-provided unit '%s'.", tab_unit, cm_unit)
 
             if not isinstance(cm_unit, u.Unit):
                 cm_unit = u.Unit(cm_unit)
-            if cm_unit.physical_type in ('length', 'frequency'):
+            if cm_unit.physical_type in ('length', 'frequency', 'energy'):
                 # Spectral axis column information
                 kwarg_val = kwarg_val.to(cm_unit, equivalencies=u.spectral())
-            elif 'spectral flux' in cm_unit.physical_type:
+            elif 'spectral flux' in str(cm_unit.physical_type):
                 # Flux/error column information
-                kwarg_val = kwarg_val.to(
-                    cm_unit, equivalencies=u.spectral_density(1 * u.AA))
+                kwarg_val = kwarg_val.to(cm_unit, equivalencies=u.spectral_density(1 * u.AA))
         elif tab_unit:
             # The user has provided no unit in the column mapping, so we
             # use the unit as defined in the table object.
diff --git a/specutils/tests/test_loaders.py b/specutils/tests/test_loaders.py
index 13fb2ee..2a597d3 100644
--- a/specutils/tests/test_loaders.py
+++ b/specutils/tests/test_loaders.py
@@ -522,14 +522,15 @@ def test_tabular_fits_writer(tmpdir, spectral_axis):
         spectrum.write(tmpfile, format='tabular-fits')
     spectrum.write(tmpfile, format='tabular-fits', overwrite=True)
 
-    cmap = {spectral_axis: ('spectral_axis', wlu[spectral_axis]),
+    # Map to alternative set of units
+    cmap = {spectral_axis: ('spectral_axis', 'micron'),
             'flux': ('flux', 'erg / (s cm**2 AA)'),
             'uncertainty': ('uncertainty', None)}
 
     # Read it back again and check against the original
     spec = Spectrum1D.read(tmpfile, format='tabular-fits', column_mapping=cmap)
     assert spec.flux.unit == u.Unit('erg / (s cm**2 AA)')
-    assert spec.spectral_axis.unit == spectrum.spectral_axis.unit
+    assert spec.spectral_axis.unit == u.um
     assert quantity_allclose(spec.spectral_axis, spectrum.spectral_axis)
     assert quantity_allclose(spec.flux, spectrum.flux)
     assert quantity_allclose(spec.uncertainty.quantity,
@@ -565,14 +566,14 @@ def test_tabular_fits_multid(tmpdir, ndim, spectral_axis):
     assert quantity_allclose(spec.uncertainty.quantity,
                              spectrum.uncertainty.quantity)
 
-    # Test again, using `column_mapping` to convert to different flux unit
-    cmap = {spectral_axis: ('spectral_axis', wlu[spectral_axis]),
+    # Test again, using `column_mapping` to convert to different spectral axis and flux units
+    cmap = {spectral_axis: ('spectral_axis', 'THz'),
             'flux': ('flux', 'erg / (s cm**2 AA)'),
             'uncertainty': ('uncertainty', None)}
 
     spec = Spectrum1D.read(tmpfile, format='tabular-fits', column_mapping=cmap)
     assert spec.flux.unit == u.Unit('erg / (s cm**2 AA)')
-    assert spec.spectral_axis.unit == spectrum.spectral_axis.unit
+    assert spec.spectral_axis.unit == u.THz
     assert quantity_allclose(spec.spectral_axis, spectrum.spectral_axis)
     assert quantity_allclose(spec.flux, spectrum.flux)
     assert quantity_allclose(spec.uncertainty.quantity,
